package com.example.mrt.sqlite;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Mr.T on 6/2/2016.
 */
public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.MyViewHolder> {
    private List<Movie> moviesList;
    private List<Book> bookList;

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.movie_list_row, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

//        Movie movie = moviesList.get(position);
        Book book = bookList.get(position);
        holder.title.setText(book.getTitle());
//        holder.genre.setText(movie.getGenre());
//        holder.year.setText(movie.getYear());

    }

    @Override
    public int getItemCount() {
        return bookList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView title, year, genre;

        public MyViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            year = (TextView) itemView.findViewById(R.id.year);
            genre = (TextView) itemView.findViewById(R.id.genre);
        }
    }

    //    public MoviesAdapter(List<Movie> moviesList) {
//        this.moviesList = moviesList;
//    }
    public MoviesAdapter(List<Book> bookList) {
        this.bookList = bookList;
    }

    public List<Book> getData() {
        return bookList;
    }

}
