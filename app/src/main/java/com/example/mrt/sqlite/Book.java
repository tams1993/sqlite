package com.example.mrt.sqlite;

import com.orm.SugarRecord;

/**
 * Created by Mr.T on 6/2/2016.
 */
public class Book extends SugarRecord {

    String title;
    String edition;

    public Book() {
    }

    public Book(String title, String edition) {
        this.title = title;
        this.edition = edition;
    }

    public String getTitle() {
        return title;
    }

    public String getEdition() {
        return edition;
    }
}
