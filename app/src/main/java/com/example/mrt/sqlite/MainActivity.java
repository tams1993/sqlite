package com.example.mrt.sqlite;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private List<Movie> movieList = new ArrayList<>();
    private RecyclerView recyclerView;
    private MoviesAdapter moviesAdapter;
    private List<Book> bookList = new ArrayList<>();
    private Button button, btnDelete;
    private EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        Toolbar toolbar = findViewById(R.id.toolbar)

        button = (Button) findViewById(R.id.button);
        btnDelete = (Button) findViewById(R.id.btnDelete);
        editText = (EditText) findViewById(R.id.editText);



        moviesAdapter = new MoviesAdapter(bookList);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(moviesAdapter);

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Book book = bookList.get(position);
                Toast.makeText(getApplicationContext(), book.getTitle() + " is selected!", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));


        preprareMovieData();


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (editText.toString() != null) {
                    SaveToDatabase(editText.getText().toString(),moviesAdapter, recyclerView);



                }
                else
                    Toast.makeText(getParent(),"Please Type Something",Toast.LENGTH_LONG).show();

            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                        Book.deleteAll(Book.class);
                moviesAdapter.getData().clear();
                moviesAdapter.getData().addAll(Book.listAll(Book.class));
                moviesAdapter.notifyDataSetChanged();
                recyclerView.invalidate();

            }
        });


    }

    private void SaveToDatabase(String Title,MoviesAdapter adapter,RecyclerView recyclerView) {

        if (searchBook(Title).isEmpty()) {
            new Book(Title, null).save();
            adapter.getData().clear();
            adapter.getData().addAll(Book.listAll(Book.class));

        } else

            Toast.makeText(this, Title + " Already exist", Toast.LENGTH_LONG).show();


        adapter.notifyDataSetChanged();
        recyclerView.invalidate();


    }

    private List<Book> searchBook(String bookName) {

        return Book.find(Book.class, "title = ?", bookName);

    }

    private void preprareMovieData() {


        bookList.addAll(Book.listAll(Book.class));
        Log.d("List", "List size " + bookList.size());




        moviesAdapter.notifyDataSetChanged();
    }

    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private MainActivity.ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerview, final MainActivity.ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {


                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerview.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerview.indexOfChild(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }
}
